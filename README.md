# bino.editable

Jquery plugin for editable html content, inspired by https://github.com/vitalets/x-editable

# Dependency
- jquery
# Installation

```<script type="text/javascript" src="/path/to/your/assets/bino.editable.js"></script>```

# Usage
```
    <div id="your-element" data-type="text" data-field="media" data-type="text">Content</div>
    
    $('#your-element').binoEditable();
```
Use `form-control` class on every input
# API Documentation
## Available options
- **data: null**

    data to send
- **origin: null**

    origin value
- **field: null**

    name of field
- **value: null**

    value wich paired with field
- **type: null**

    type of input ['text', 'textarea', 'select'] or function
- **required: true**

    is this value required?
- **requiredError: null**

    function(input, element){};
- **url: null**

    url to post
- **method: 'post'**

    ajax method
- **onClick: null**

    callback on click input element function(input, element){};
- **event: null**

    get event
- **success: null**

    ajax success callback function(input, element, result){};
- **done: null**

    ajax done callback function(input, element){};
- **error: null**

    ajak error callback function(input, element){};
- **beforeSend: null**

    ajax before send callback function(input, element){};


# Sample
## Using external input modifier plugin
    <span class="editable-media" data-data-id="123" data-field="test" data-type="select">Test</span>

    <script type="text/javascript" src="/path/to/selectize.min.js"></script>
    <script type="text/javascript" src="/path/to/your/assets/bino.editable.js"></script>

    $('.editable').on('click', function () {
        $(this).binoEditable({
            url: 'http://yourdomain.com/post',
            data: {
                id: $(this).data('data-id'),
            },
            onClick: loadSomething,
            error: errorUpdateField,
            success: successUpdateField
        });
    });


    function loadSomething(input, elem) {
        input.selectize({
            valueField: 'json',
            labelField: 'name',
            searchField: 'name',
            options: [],
            create: false,
            render: {
                option: function(item, escape) {
                    return '<div class="selectize-item">' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.name) + ' [' + escape(item.media.name) + ']</span>' +
                        '</span>' +
                        '</div>';
                }
            },
            load: function(query, callback) {
                if (!query.length) return callback();
                $.ajax({
                    url: 'http://yourdomain.com/path/to/json',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        q: query,
                        media_id: elem.data('media-id')
                    },
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                        callback(res);
                    }
                });
            }
        });
    }

    function errorUpdateField(input, elem) {
        alert('Update '+elem.data('field')+' Error');
    }

    function successUpdateField(input, elem, result) {
        console.log(result)
    }


# License
Copyright 2019 [Gilang](https://gitlab.com/abadigilang)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
