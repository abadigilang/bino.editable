(function ($) {
    $.fn.binoEditable = function (options) {
        // This is the easiest way to have default options.
        let settings = $.extend({
            data: null,
            origin: null,
            value: null,
            type: null,
            field: null,
            required: true,
            requiredError: null,
            url: null,
            method: 'post',
            onClick: null,
            event: null,
            success: null,
            done: null,
            error: null,
            beforeSend: null,
        }, options );

        let that = this;
        let elem, input, prevVal, name;

        that.value = settings.value;
        elem = $(this);

        if (!settings.type) {
            settings.type = elem.data('type');
        }

        if (!settings.field) {
            settings.field = elem.data('field');
        }

        if (!settings.value) {
            if ($(this).data('value')) {
                settings.value = $(this).data('value');
            } else {
                settings.value = elem.text();
            }
        }

        if (!settings.origin) {
            settings.origin = elem.data('origin');
        }

        if (settings.origin) {
            settings.value = settings.origin;
        }

        if ($(this).data('field')) {
            name = $(this).data('field');
        }

        prevVal = settings.value;

        input = $('<input>');
        buttonOk = $('<button>').addClass('btn btn-info');
        buttonNo = $('<button>').addClass('btn btn-gray');
        buttonOk.html('<i class="fa fa-check"></i>');
        buttonNo.html('<i class="fa fa-close"></i>');

        elem.hide();
        switch (settings.type) {
            case 'text':
                input.addClass('form-control').attr('type', 'text').val(settings.value);
                break;
            case 'textarea':
                input = $('<textarea rows="10">');
                input.addClass('form-control').val(settings.value);
                break;
            case 'select':
                input = $('<select class="form-control">');
                input.addClass('form-control').val(settings.value);
                break;
            default:
                elem.hide();
                input.addClass('form-control').attr('type', 'text').val(settings.value);
        }

        input.attr("name", settings.field);

        let wrapper = $('<div class="form-group">').append(input, $('<div>').addClass('form-group text-right').append(buttonOk, buttonNo));
        wrapper.insertAfter(elem);

        if (settings.onClick) {
            settings.onClick(input, elem);
        }

        buttonOk.on('click', function () {
            elem.trigger('onSubmit');
            if (settings.onSubmit) {
                settings.onSubmit();
            }

            if (settings.required) {
                if (input.val().length < 1) {
                    if (!settings.requiredError) {
                        return alert(settings.field + " can't be null.");
                    } else {
                        return settings.requiredError(input, elem);
                    }
                }
            }

            that.value = input.val();
            // $(this).closest(input).remove();
            $(this).closest(wrapper).remove();
            // input.remove();

            if (that.value !== prevVal) {
                if (name) {
                    settings.data[name] = that.value;
                }
                __submit();
            }
            //
            // elem.text(that.value);
            // elem.show();
        });

        buttonNo.on('click', function () {
            elem.show();
            // $(this).closest(input).remove();
            $(this).closest(wrapper).remove();
        });

        function __submit() {
            $.ajax({
                method: settings.method,
                data: settings.data,
                url: settings.url,
                done: function () {
                    elem.trigger('ajax.done');
                    elem.text(that.value);
                    elem.show();

                    if (settings.done) {
                        settings.done(input, elem);
                    }
                },
                success: function (result) {
                    elem.text(that.value);
                    elem.show();

                    if (settings.success) {
                        settings.success(input, elem, result);
                    }
                    elem.trigger('ajax.success');
                },
                error: function () {
                    if (settings.error) {
                        settings.error(input, elem);
                    }
                    elem.trigger('ajax.error');
                    elem.show();
                    return this;
                },
                beforeSend: function() {
                    if (settings.beforeSend) {
                        settings.beforeSend(input, elem);
                    }
                    elem.trigger('ajax.beforesend');
                }
            });
        }

        return this;
    };
})(jQuery);
